const express = require('express');
const upload = require('../upload');
const auth = require('../middleware/auth');
const Post = require('../models/Post');

const router = express.Router();

router.get('/', (req, res) => {
  Post.find({}, null, {sort: {date: 'DESC'}}).populate('user').lean()
    .then(posts => {
      const result = posts.map(post => {
        delete post.description;

        return {
          ...post,
          user: post.user.username
        };
      });

      res.send(result);
    })
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Post.findById(req.params.id).populate('user').lean()
    .then(post => {
      if (post) {
        const result = {
          ...post,
          user: {
            _id: post.user._id,
            username: post.user.username
          }
        };

        res.send(result);
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', auth, upload.single('image'), (req, res) => {
  const postData = req.body;

  if (req.file) {
    postData.image = req.file.filename;
  }

  postData.user = req.user._id;
  postData.date = new Date().toISOString();

  const post = new Post(postData);

  post.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});


module.exports = router;
