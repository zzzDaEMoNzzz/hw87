const express = require('express');
const auth = require('../middleware/auth');
const Comment = require('../models/Comment');

const router = express.Router();

router.get('/:post', (req, res) => {
  Comment.find({post: req.params.post}, null, {sort: {date: 'DESC'}}).populate('user').lean()
    .then(comments => {
      const result = comments.map(comment => ({
        ...comment,
        user: {
          _id: comment.user._id,
          username: comment.user.username
        }
      }));

      res.send({
        count: result.length,
        comments: result
      });
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', auth, (req, res) => {
  const comment = new Comment(req.body);

  comment.user = req.user._id;
  comment.date = new Date().toISOString();

  comment.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

module.exports = router;