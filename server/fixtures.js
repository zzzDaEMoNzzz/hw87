const mongoose = require('mongoose');
const config = require('./config');

const Post = require('./models/Post');
const Comment = require('./models/Comment');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [user1, user2] = await User.create(
    {
      username: 'John',
      password: '123',
      token: '12345'
    },
    {
      username: 'Jack',
      password: '123',
      token: '54321'
    }
  );

  const [post1, post2, post3] = await Post.create(
    {
      user: user1._id,
      title: 'Post1',
      description: 'Description will be here',
      date: new Date().toISOString()
    },
    {
      user: user1._id,
      title: 'Post2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n' +
        'Asperiores culpa distinctio excepturi, facere molestias perspiciatis similique totam vero.\n' +
        'Nulla, provident, tempora. Dolor eos exercitationem iste molestias nam placeat quod veniam.',
      image: '_album_five.jpg',
      date: new Date().toISOString()
    },
    {
      user: user2._id,
      title: 'Post3',
      image: '_album_day_of_the_dead.jpg',
      date: new Date().toISOString()
    }
  );

  await Comment.create(
    {user: user1._id, post: post1._id, description: 'Some commentary\nLine2\nLine3', date: new Date().toISOString()},
    {user: user2._id, post: post1._id, description: 'Another commentary', date: new Date().toISOString()},

    {user: user2._id, post: post2._id, description: 'Comment1', date: new Date().toISOString()},
    {user: user2._id, post: post2._id, description: 'Commentary 2', date: new Date().toISOString()},

    {user: user1._id, post: post3._id, description: 'Some text', date: new Date().toISOString()},
    {user: user1._id, post: post3._id, description: 'Test comment', date: new Date().toISOString()}
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});