const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    validate: {
      validator: async function() {
        if(!this.description && !this.image) throw new Error();
      },
      message: 'Image or description must be filled'
    }
  },
  image: {
    type: String,
    validate: {
      validator: async function() {
        if(!this.description && !this.image) throw new Error();
      },
      message: 'Image or description must be filled'
    }
  },
  date: {
    type: String,
    required: true
  }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;
