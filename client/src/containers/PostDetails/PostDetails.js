import React, {Component, Fragment} from 'react';
import moment from "moment";
import {connect} from "react-redux";
import {cleanPostInfoPage, fetchPostInfo} from "../../store/actions/postsActions";
import './PostDetails.css';
import {apiURL} from "../../constants";
import {fetchComments} from "../../store/actions/commentsActions";
import Comments from "../../components/Comments/Comments";
import AddComment from "../AddComment/AddComment";

class PostDetails extends Component {
  componentDidMount() {
    const postID = this.props.match.params.post;

    this.props.fetchPostInfo(postID);
    this.props.fetchComments(postID);
  }

  componentWillUnmount() {
    this.props.cleanPostInfoPage();
  }

  render() {
    if (!this.props.postInfo) return <h2>Loading...</h2>;

    const date = moment(this.props.postInfo.date).format('DD.MM.YYYY HH:mm');

    return (
      <Fragment>
        <div className="PostDetails">
          <div className="PostDetails-header">
            <span className="PostDetails-headerDate">{date}</span>
            <span>by {this.props.postInfo.user.username}</span>
          </div>
          <h2>{this.props.postInfo.title}</h2>
          {this.props.postInfo.image && (
            <img
              src={`${apiURL}/uploads/${this.props.postInfo.image}`}
              alt=""
              className="PostDetails-image"
            />
          )}
          {this.props.postInfo.description && (
            <p>{this.props.postInfo.description}</p>
          )}
          <div className="PostDetails-footer">
            <span>{this.props.comments.count} comments</span>
          </div>
        </div>
        {this.props.comments.count > 0 && (
          <Comments data={this.props.comments.comments}/>
        )}
        {this.props.user && (
          <AddComment post={this.props.match.params.post}/>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  postInfo: state.posts.postInfo,
  comments: state.comments.comments,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  fetchPostInfo: id => dispatch(fetchPostInfo(id)),
  cleanPostInfoPage: () => dispatch(cleanPostInfoPage()),
  fetchComments: postId => dispatch(fetchComments(postId))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);