import React, {Component} from 'react';
import {connect} from "react-redux";
import './AddPost.css';
import {createPost} from "../../store/actions/postsActions";

class AddPost extends Component {
  state = {
    title: '',
    description: '',
    file: null,
    fileName: null
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.createPost(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      file: event.target.files[0],
      fileName: event.target.files[0] && event.target.files[0].name
    })
  };

  render() {
    return (
      <form
        className="AddPost"
        onSubmit={this.submitFormHandler}
      >
        <label htmlFor="title">Title</label>
        <input
          type="text"
          id="title"
          name="title"
          onChange={this.inputChangeHandler}
        />
        <label htmlFor="description">Description</label>
        <textarea
          name="description"
          id="description"
          onChange={this.inputChangeHandler}
          rows="5"
        />
        <label htmlFor="image" className="AddPost-uploadFile">
          <span className="AddPost-uploadLabelName">Image</span>
          <div className="AddPost-uploadBtn">Upload file</div>
          <span className="AddPost-fileName">{this.state.fileName}</span>
          <input
            type="file"
            id="image"
            name="image"
            onChange={this.fileChangeHandler}
          />
        </label>
        <button className="AddPost-postBtn">Create post</button>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  createPost: postData => dispatch(createPost(postData))
});

export default connect(null, mapDispatchToProps)(AddPost);