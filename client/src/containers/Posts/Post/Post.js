import React from 'react';
import moment from 'moment';
import {Link} from "react-router-dom";
import './Post.css';
import chatIcon from '../../../assetts/ic_chat.png';
import {apiURL} from "../../../constants";

const Post = ({data}) => {
  const imageUrl = data.image ? `${apiURL}/uploads/${data.image}` : chatIcon;

  return (
    <Link to={`/posts/${data._id}`} className="Post">
      <img src={imageUrl} style={data.image ? null : {border: 'none'}} alt=""/>
      <div>
        <h4>
          <span className="Post-date">{moment(data.date).format('DD.MM.YYYY HH:mm')}</span>
          <span className="Post-user">by {data.user}</span>
        </h4>
        <p className="Post-title">{data.title}</p>
      </div>
    </Link>
  );
};

export default Post;
