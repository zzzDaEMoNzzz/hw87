import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import Post from "./Post/Post";

class Posts extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    const posts = this.props.posts.map(postData => <Post key={postData._id} data={postData}/>);

    return (
      <div>
        {posts}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts
});

const mapDispatchToProps = dispatch => ({
  getPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);