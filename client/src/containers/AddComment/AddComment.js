import React, {Component} from 'react';
import {connect} from "react-redux";
import './AddComment.css';
import {addComment} from "../../store/actions/commentsActions";

class AddComment extends Component {
  state = {
    comment: ''
  };

  addComment = event => {
    event.preventDefault();

    this.props.addComment({
      post: this.props.post,
      description: this.state.comment
    }).then(() => this.setState({comment: ''}));
  };

  onCommentChange = event => {
    this.setState({comment: event.target.value});
  };

  render() {
    return (
      <form className="AddComment" onSubmit={this.addComment}>
        <textarea
          rows="5"
          value={this.state.comment}
          onChange={this.onCommentChange}
          required
        />
        <button>Add comment</button>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addComment: commentData => dispatch(addComment(commentData))
});

export default connect(null, mapDispatchToProps)(AddComment);