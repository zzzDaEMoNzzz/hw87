import axios from 'axios';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const fetchComments = postId => {
  return dispatch => {
    axios.get(`/comments/${postId}`).then(
      response => dispatch(fetchCommentsSuccess(response.data))
    )
  };
};

export const addComment = commentData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    return axios.post('/comments', commentData, {headers: {Authorization: token}}).then(
      () => dispatch(fetchComments(commentData.post))
    )
  };
};