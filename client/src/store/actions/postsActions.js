import axios from 'axios';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';

const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});

export const fetchPosts = () => {
  return dispatch => {
    axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    )
  };
};

export const createPost = postData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    axios.post('/posts', postData, {headers: {Authorization: token}}).then(
      () => dispatch(push('/posts')),
      error => {
        if (error.response) {
          const errorData = error.response.data;
          const errorMessage = errorData.error || errorData.errors[Object.keys(errorData.errors)[0]].message;

          if (errorMessage) {
            NotificationManager.error(errorMessage);
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    )
  }
};

export const FETCH_POST_INFO_SUCCESS = 'FETCH_POST_INFO_SUCCESS';

const fetchPostsInfoSuccess = postInfo => ({type: FETCH_POST_INFO_SUCCESS, postInfo});

export const fetchPostInfo = id => {
  return dispatch => {
    axios.get(`/posts/${id}`).then(
      response => dispatch(fetchPostsInfoSuccess(response.data))
    )
  };
};

export const CLEAN_POST_INFO_PAGE = 'CLEAN_POST_INFO_PAGE';

export const cleanPostInfoPage = () => ({type: CLEAN_POST_INFO_PAGE});