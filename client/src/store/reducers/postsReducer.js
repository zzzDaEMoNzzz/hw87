import {
  CLEAN_POST_INFO_PAGE,
  FETCH_POST_INFO_SUCCESS,
  FETCH_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
  posts: [],
  postInfo: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case FETCH_POST_INFO_SUCCESS:
      return {...state, postInfo: action.postInfo};
    case CLEAN_POST_INFO_PAGE:
      return {...state, postInfo: null};
    default:
      return state;
  }
};

export default reducer;