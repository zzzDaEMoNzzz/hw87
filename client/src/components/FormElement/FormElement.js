import React, {Component, Fragment} from 'react';
import './FormElement.css';

class FormElement extends Component {
  state = {
    errorMessage: null
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.error !== prevProps.error) {
      this.setState({errorMessage: this.props.error});
    }
  }

  onInputChange = event => {
    this.setState({errorMessage: null});

    this.props.onChange(event);
  };

  render() {
    return (
      <Fragment>
        <label htmlFor={this.props.propertyName}>
          {this.props.title}
          {this.state.errorMessage && (
            <span className="FormElement-errorMessage">{this.state.errorMessage}</span>
          )}
        </label>
        <input
          className={'FormElement-input' + (this.state.errorMessage ? ' FormElement-inputError' : '')}
          type={this.props.type}
          id={this.props.propertyName}
          name={this.props.propertyName}
          value={this.props.value}
          onChange={this.onInputChange}
          required={this.props.required}
        />
      </Fragment>
    );
  }
}

export default FormElement;