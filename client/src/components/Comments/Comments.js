import React from 'react';
import moment from 'moment';
import './Comments.css';

const Comments = ({data}) => {
  const comments = data.map(comment => (
    <div key={comment._id} className="Comment">
      <div className="Comment-header">
        <span className="Comment-headerDate">{moment(comment.date).format('DD.MM.YYYY HH:mm:ss')}</span>
        <span>by {comment.user.username}</span>
      </div>
      <p>{comment.description}</p>
    </div>
  ));

  return (
    <div className="Comments">
      {comments}
    </div>
  );
};

export default Comments;
