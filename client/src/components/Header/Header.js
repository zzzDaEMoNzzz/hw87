import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import './Header.css';

const Header = ({user, logout}) => {
  const anonymBar = (
    <nav>
      <NavLink to="/register">Register</NavLink>
      <span>or</span>
      <NavLink to="/login">Login</NavLink>
    </nav>
  );

  const userBar = (
    <nav>
      <span>Hello, <strong>{user && user.username}</strong>!</span>
      <NavLink to="/posts/add">Add new post</NavLink>
      <span>or</span>
      <button className="Header-logoutBtn" onClick={logout}>Logout</button>
    </nav>
  );

  return (
    <div className="Header">
      <Link to="/" className="Header-logo">Forum</Link>
      {user ? userBar : anonymBar}
    </div>
  );
};

export default Header;
